package edu.cei.hilos;

/**
 * Hello world!
 *
 */
public class App2 {
	public static void main(String[] args) throws InterruptedException {

		System.out.println("Hello World!");
		SingletonCaller singletonCaller1 = new SingletonCaller("Singleton Caller 1", 5000900L);
		SingletonCaller singletonCaller2 = new SingletonCaller("Singleton Caller 2", 5000902L);
		
		singletonCaller1.start();
		singletonCaller2.start();
		
		Thread.sleep(10000000L);
		System.out.println("Listo?");
	}
}
