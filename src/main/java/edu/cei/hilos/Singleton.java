package edu.cei.hilos;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Singleton {

	private static final Logger LOGGER = LoggerFactory.getLogger(Singleton.class);

	private static Singleton instance;
	
	public synchronized static Singleton getInstance() {
		if(Singleton.instance == null) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				LOGGER.error(e.getMessage(), e);
			}
			Singleton.instance = new Singleton();
		}
		return Singleton.instance;
	}
	
	private Singleton() {}
}
